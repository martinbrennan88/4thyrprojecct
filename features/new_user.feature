Feature: Creating projects
	In order to have pokergames you need to have a user
	As a user
	I want to create them easily

Background:
    Given the database is clean
    And I am on the homepage
    When I follow "Sign up"

Scenario: Creating a project
	When I fill in the following:
    	| First name 		| dave       |
    	| Last name     	| tester |
    	| Username      	| Nice_guy   |
    	| Email				| dave@hotmail.com|
    	| Password   		|   12345678         |
        | Password confirmation | 12345678 |
	And I press "Submit"
	And I should see "Welcome! You have signed up successfully."