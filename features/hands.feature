Feature: PokerHands all variety of poker hands to 
	Testing all variety of poker hands to ensure correct result
    
Background:
    Given the database is clean
    And the database is up to date
    And I have loaded users

Scenario: straight flush
    Given I have a game
    And I Have 2 users
    When the users are dealt the following cards
        | 14,d  | 13,d |
        | 13,s  | 13,c |  
    And the flop turn and river are
        | 12,d      | 11,d      |
        | 14,s      | 14,h      |
        | 10,d      |           |
    Then the hand rank should be "straight flush"
    And the winning hand should be 
        | 14,d      | 13,d      |
        | 12,d      | 11,d      |
        | 10,d      |           |

Scenario: Four of a kind
    Given I have a game
    And I Have 2 users
	When the users are dealt the following cards
    	| 14,c	| 14,d |
        | 13,s  | 13,c |  
	And the flop turn and river are
        | 13,d      | 11,s      |
        | 14,s      | 14,h      |
        | 10,c      |           |
	Then the hand rank should be "four of a kind"
    And the winning hand should be 
        | 14,c      | 14,d      |
        | 14,s      | 14,h      |
        | 13,d      |           |

Scenario: fullhouse
    Given I have a game
    And I Have 2 users
    When the users are dealt the following cards
        | 14,c  | 14,d |
        | 13,s  | 13,c |  
    And the flop turn and river are
        | 13,d      | 11,s      |
        | 11,d       | 14,h      |
        | 10,c      |           |
    Then the hand rank should be "fullhouse"
    And the winning hand should be 
        | 14,c      | 14,d      |
        | 14,h      | 11,s      |
        | 11,d      |           |

Scenario: flush
    Given I have a game
    And I Have 2 users
    When the users are dealt the following cards
        | 12,d  | 14,c |
        | 6,c  | 13,c |  
    And the flop turn and river are
        | 13,d      | 11,s      |
        | 11,c      | 7,c      |
        | 10,c      |           |
    Then the hand rank should be "flush"
    And the winning hand should be 
        | 13,c      | 11,c      |
        | 7,c      | 10,c      |
        | 6,c      |           |

Scenario: straight
    Given I have a game
    And I Have 2 users
    When the users are dealt the following cards
        | 14,c  | 14,d |
        | 10,s  | 9,c |  
    And the flop turn and river are
        | 8,d      | 7,s      |
        | 7,s       | 6,h      |
        | 10,c      |           |
    Then the hand rank should be "straight"
    And the winning hand should be 
        | 10,c      | 9,c      |
        | 8,d      | 7,s     |
        | 6,h     |           |


Scenario: low straight
    Given I have a game
    And I Have 2 users
    When the users are dealt the following cards
        | 14,c  | 14,d |
        | 14,s  | 2,c |  
    And the flop turn and river are
        | 8,d      | 3,s      |
        | 7,s       | 4,h      |
        | 5,c      |           |
    Then the hand rank should be "straight"
    And the winning hand should be 
        | 14,s     | 5,c      |
        | 4,h      | 3,s     |
        | 2,c     |           |


Scenario: three of a kind
    Given I have a game
    And I Have 2 users
    When the users are dealt the following cards
        | 14,c  | 14,d |
        | 13,s  | 13,c |  
    And the flop turn and river are
        | 13,d      | 11,s      |
        | 7,s       | 14,h      |
        | 10,c      |           |
    Then the hand rank should be "three of a kind"
    And the winning hand should be 
        | 14,c      | 14,d      |
        | 14,h      | 11,s      |
        | 13,d      |           |

Scenario: two pair
    Given I have a game
    And I Have 2 users
    When the users are dealt the following cards
        | 5,c  | 14,d |
        | 8,s  | 11,c |  
    And the flop turn and river are
        | 4,d      | 11,s      |
        | 7,s       | 7,h      |
        | 10,c      |           |
    Then the hand rank should be "two pair"
    And the winning hand should be 
        | 11,c      | 11,s      |
        | 7,h      | 7,s      |
        | 10,c      |           |

Scenario: pair
    Given I have a game
    And I Have 2 users
    When the users are dealt the following cards
        | 14,c  | 14,d |
        | 13,s  | 13,c |  
    And the flop turn and river are
        | 11,d      | 5,s      |
        | 7,s       | 6,h      |
        | 10,c      |           |
    Then the hand rank should be "pair"
    And the winning hand should be 
        | 14,c      | 14,d      |
        | 10,c       |   7,s     |
        | 11,d      |           |

Scenario: highcard
    Given I have a game
    And I Have 2 users
    When the users are dealt the following cards
        | 9,c  | 2,d |
        | 4,s  | 3,c |  
    And the flop turn and river are
        | 6,d      | 11,s      |
        | 7,s       | 12,h      |
        | 10,c      |           |
    Then the hand rank should be "high card"
    And the winning hand should be 
        | 12,h      | 11,s      |
        | 10,c      | 9,c      |
        | 7,s      |           |

Scenario: StateMachine
    Given I have a game
    And I Have 2 users
    And State is "waiting"
    When I change State 1 times
    Then State should be "new_round"

Scenario: StateMachine
    Given I have a game
    And I Have 2 users
    And State is "waiting"
    When I change State 2 times
    Then State should be "deal_flop"

Scenario: StateMachine
    Given I have a game
    And I Have 2 users
    And State is "waiting"
    When I change State 3 times
    Then State should be "deal_turn"

Scenario: StateMachine
    Given I have a game
    And I Have 2 users
    And State is "waiting"
    When I change State 4 times
    Then State should be "deal_river"

Scenario: StateMachine
    Given I have a game
    And I Have 2 users
    And State is "waiting"
    When I change State 5 times
    Then State should be "hand_over"
