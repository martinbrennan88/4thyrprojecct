class AddReadyToUser < ActiveRecord::Migration
  def change
    add_column :users, :ready, :integer
  end
end
