class AddDeckToGame < ActiveRecord::Migration
  def change
    add_column :games, :deck, :string
  end
end
