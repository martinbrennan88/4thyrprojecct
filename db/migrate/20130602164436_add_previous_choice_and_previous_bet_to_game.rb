class AddPreviousChoiceAndPreviousBetToGame < ActiveRecord::Migration
  def change
    add_column :games, :previous_bet, :integer
    add_column :games, :previous_choice, :string
  end
end
