class AddChoiceAndActiveuserToGame < ActiveRecord::Migration
  def change
    add_column :games, :activeuser, :integer
    add_column :games, :choice, :string
  end
end
