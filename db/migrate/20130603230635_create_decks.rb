class CreateDecks < ActiveRecord::Migration
  def change
    create_table :decks do |t|
      t.string :pokerdeck
      t.string :communal_cards
      t.integer :game_id

      t.timestamps
    end
  end
end
