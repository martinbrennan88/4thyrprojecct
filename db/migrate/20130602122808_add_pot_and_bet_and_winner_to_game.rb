class AddPotAndBetAndWinnerToGame < ActiveRecord::Migration
  def change
    add_column :games, :pot, :integer
    add_column :games, :bet, :integer
    add_column :games, :winner, :integer
  end
end
