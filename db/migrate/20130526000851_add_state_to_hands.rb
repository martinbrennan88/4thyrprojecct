class AddStateToHands < ActiveRecord::Migration
  def change
    add_column :hands, :state, :integer
  end
end
