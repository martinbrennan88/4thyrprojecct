class UsersController < ApplicationController

before_filter :authenticate_user!
    def index
        @users = User.all
        respond_to do |format|
            format.json { render json: @users }
        end
    end
  
    # GET /users/1
    # GET /users/1.json
    def show
        @user = User.find_by_id(current_user.id)
    end
  
    # GET /users/new
    # GET /users/new.json
    def new
        @user = User.new
    
        respond_to do |format|
            format.html # new.html.erb
            format.json { render json: @user }
        end
    end
  
    # GET /users/1/edit
    def edit
      @user = User.find(params[:id])
    end
  
    # POST /users
    # POST /users.json
    def create
        @user = User.new(params[:user])
    
        respond_to do |format|
            if @user.save
                format.html { redirect_to @user, notice: 'User was successfully created.' }
                format.json { render json: @user, status: :created, location: @user }
            else
                format.html { render action: "new" }
                format.json { render json: @user.errors, status: :unprocessable_entity }
            end
        end
    end


    def get_hand
        require 'debugger' ; debugger
        @user = User.find_by_id(current_user.id)
        hand = @user.hands.pop()
         respond_to do |format|
            format.json { render json: hand}
        end
    end
  
    # PUT /users/1
    # PUT /users/1.json
    def update
        @user = User.find(params[:id])
        respond_to do |format|
            if @user.update_attributes(params[:user])
                format.html { redirect_to @user, notice: 'User was successfully updated.' }
                format.json { head :no_content }
            else
                format.html { render action: "edit" }
                format.json { render json: @user.errors, status: :unprocessable_entity }
            end
        end
    end

    def top_up
        @user = User.find_by_id(current_user.id)
        respond_to do |format|
            unless @user.balance == 1000
                @user.top_up
                format.html { redirect_to @user, notice: 'Accounted topped up to 1000 happy playing.' }
            else
                format.html { redirect_to @user, notice: 'dont be greedy now you already have 1000 or over!' }
            end
        end
    end
  
    # DELETE /users/1
    # DELETE /users/1.json
    def destroy
        @user = User.find(params[:id])
        @user.destroy
    
        respond_to do |format|
            format.html { redirect_to users_url }
            format.json { head :no_content }
        end
    end
end
