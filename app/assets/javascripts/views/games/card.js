Project.Views.CommunalCard =  Backbone.View.extend({
	template: JST['hand'],

	
	initialize: function(model, container){
		_.bindAll(this, 'render');
  		this.model.on('change', this.render, this);
		this.container = container;
	},
	
	render: function() {
		this.add_cards();
    	$(this.el).html(this.template({model: this.model}, this.path_one, this.path_two));
    	return this;
  	},
});