Project.Views.Hand =  Backbone.View.extend({
	template: JST['hand'],

	
	initialize: function(model, container){
		_.bindAll(this, 'render');
   //this.model.on('change', this.render, this);
		this.container = container;
	},
	
	render: function() {
		this.add_cards();
    	$(this.el).html(this.template({model: this.model}, this.path_one, this.path_two));
    	return this;
  	},

  	
// selects the first holecard and second hole card from users hand and gets the path to them in the ruby project
  	add_cards: function() {
  		var holeCards = this.model.get('hole_cards');
  		var firstHoleCard = holeCards[0];
  		var secondHoleCard = holeCards[1];
  		this.path_one = App.assets.path('cards_png/' + firstHoleCard.suit + firstHoleCard.number +'.png');
  		this.path_two = App.assets.path('cards_png/' + secondHoleCard.suit + secondHoleCard.number +'.png');
  	}
});