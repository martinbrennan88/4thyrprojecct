Project.Views.Game =  Backbone.View.extend({
	template: JST['games/new'],


	events: {
    'click #leave': 'leave',
    'click #join': 'join'
  	},

	initialize: function(game,users){
    _.bindAll(this);
    //this.model.on('change', this.render);
		this.users = users;
    this.getHands();
    this.getCards();
    this.renderUsers();
    //this.startPollers();
	},


/// gets hands from the server
  getHands: function(){
    this.hands = new Project.Collections.Hands([],{ id: this.model.get('id')});
    this.hands.fetch({reset: true, async: false});
  },


// gets the communal cards for the server
  getCards: function(){
    this.cards = new Project.Collections.Cards([],{ id: this.model.get('id')});
    this.cards.fetch({reset: true, async: false});
  },
// renders the communal cards into a view
  render_cards: function(){
    cardsView = new Project.Views.CommunalHand({collection: this.cards});
    return $('#cards').html(cardsView.render().el);
  },


// renders this view
	render: function() {
    console.log('games render called.....');
		this.choice();
    this.renderUsers();
    this.render_cards();
		//this.hands = new Project.Collections.Hands();
	//	console.log(this.model.get('id'));
  	$(this.el).html(this.template({game: this.model},this.cards));
  	return this;
	},


  // takes the users choice in
  choice: function(){
    console.log('game choice called.....');
    this.model.get('activeuser');
    if (this.model.get('choice') === null && this.users.where({game_id: this.model.get('id')}).length === 2){
      var users = this.users.where({game_id: this.model.get('id')});
      var user = users[0];
      this.model.set({choice: 'first_user_choice', activeuser: user.get('id')});
      this.model.save();
    }
    else if (this.model.get('state') === "hand_over" && this.users.where({game_id: this.model.get('id')}).length === 2){
      alert("press next round to begin new round");
  }
  },

  

	renderUsers: function(){
    console.log("users rendered from game......2");
		// alert(JSON.stringify());
    var usersInGame = this.users.where({game_id: this.model.get('id')});
    if (usersInGame[0] !== (null || undefined)){
      this.user1 = usersInGame[0];
      viewUser1 = new Project.Views.User({model: usersInGame[0]},this.model,'#player1_container',1,
       this.hands.findWhere({user_id: usersInGame[0].get('id')}));
      viewUser1.addButtons();
      viewUser1.availableOptions();

      if (usersInGame[1] !== (null || undefined)){
        this.user2 = usersInGame[1];
        viewUser2 = new Project.Views.User({model: usersInGame[1]},this.model,'#player2_container',2,
        this.hands.findWhere({user_id: usersInGame[1].get('id')}));
        viewUser2.addButtons();
        viewUser2.availableOptions();
        //$('#player2_container').html(viewUser2.render().el);
        }
      }
	},


  leave: function(){
    alert("player left game")
    var current = this.users.findWhere({id: parseInt(window.currentUser.id)});
    current.set({game_id: 0});
    this.render();
  },

// selects the winning player from this round 
  winning_player: function(){
    this.winner = this.users.findWhere({id: this.game.get('winner')});
  },

/// when the join button is pushed user joins the game
  join: function(){
    console.log("player joined game")
    var current = this.users.findWhere({id: parseInt(window.currentUser.id)});
    if(this.users.where({game_id: this.model.get('id')}).length < 2){
      current.set({game_id: this.model.get('id')});
      current.save();
      this.render();
      $('#join').hide();
    }
    else {
      alert('table full max players two try and join a different table');
    }
  },
// starts polling the server for games users hands and cards
    startPollers: function(){
    var game_poller = Backbone.Poller.get(this.model).start();
     // game_poller.on('success', function(model){
     //    console.info('game poller is working woop woop'); 
     //  });
     //  game_poller.on('complete', function(model){
     //    console.info('hurray! we are done!');
     //  });
     //  game_poller.on('error', function(model){
     //    console.error('oops! something went wrong with game poller'); 
     //  });
    var user_poller = Backbone.Poller.get(this.users).start();
     // game_poller.on('success', function(model){
     //    console.info('user poller is working woop woop'); 
     //  });
     //  game_poller.on('complete', function(model){
     //    console.info('hurray! we are done!');
     //  });
     //  game_poller.on('error', function(model){
     //    console.error('oops! something went wrong with user poller'); 
     //  });
    var hands_poller = Backbone.Poller.get(this.hands).start();
    var cards_poller = Backbone.Poller.get(this.cards).start();
    // game_poller.on('success', function(model){
    //     console.info('hands poller is working woop woop'); 
    //   });
    //   game_poller.on('complete', function(model){
    //     console.info('hurray! we are done!');
    //   });
    //   game_poller.on('error', function(model){
    //     console.error('oops! something went wrong with hands poller'); 
    //   });
  }


 // ready: function(){
  //   var usersInGame = this.users.where({game_id: this.model.get('id')});
  //   if(usersInGame[0].get('ready') === 1 && usersInGame[1].get('ready') === 1){
  //     this.model.set('choice', "first_user_choice");
  //     usersInGame[0].set('ready', 0);
  //     usersInGame[0].save();
  //     usersInGame[1].set('ready', 0);
  //     usersInGame[1].save();
  //   }
  //   this.model.save();
  // },

  // nextHand: function(){
  //   user = this.users.findWhere({id: parseInt(window.currentUser.id), game_id: this.model.get('id')});
  //   if (user !== undefined || user!== null && game.state === "hand_over"){
  //       var ready=confirm("play next hand");
  //       if (ready==true) {
  //         user.set('ready',1);
  //         user.save();
  //         this.ready();
  //       } else {
  //         this.render();
  //       }
  //   } else {
  //     alert("theres a hand happening")
  //   }

  // },

});