Project.Routers.Games = Backbone.Router.extend({
	routes: {

    // a way to make routes appear on a sub directory /game
        '': 'games',
        ':id' : 'show'
    },

    initialize: function() {
      this.collection = new Project.Collections.Games();
      this.users = new Project.Collections.Users();
      this.users.fetch({reset: true, async: false});
      this.collection.fetch({reset: true, async: false});
    },
        
    games: function() {
    	var view;
       	view = new Project.Views.GamesIndex({collection: this.collection});
       	$('#backbone_container').html(view.render().el);
    },

    show: function(id) {
        var game = this.collection.get(id);
        
       // alert(JSON.stringify(current));
        
        view = new Project.Views.Game({model: game},this.users);
        $('#backbone_container').empty();
        $('#backbone_container').html(view.render().el);
    }
});