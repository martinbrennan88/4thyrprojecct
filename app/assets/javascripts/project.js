window.Project = {
  Models: {},
  Collections: {},
  Views: {},
  Routers: {},
  initialize: function() {
    new Project.Routers.Games();
    Backbone.history.start();
  }
};

$(document).ready(function() {
  	Project.initialize();
});
