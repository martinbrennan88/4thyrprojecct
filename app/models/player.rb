class Player

	def initialize(user)
		@id = user.id
		@player_name = user.username
		@player_cash = user.balance
	end

	def hand= hand
		@hand = hand
		@hand.user_id = @id
	end

	def hand
		@hand
	end

	def name
		@player_name
	end

	def id
		@id
	end
end
