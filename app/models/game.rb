class Game < ActiveRecord::Base

	has_many :hands
	has_many :users
	serialize :communal_cards, Array


	state_machine :initial => :waiting do
		after_transition :waiting => :new_round, :do => :deal_cards
		after_transition :hand_over => :new_round, :do => :deal_cards
		after_transition :new_round => :deal_flop, :do => :deal_flop_cards
		after_transition :deal_flop => :deal_turn, :do => :deal_turn_cards
		after_transition :deal_turn => :deal_river, :do => :deal_river_cards
		after_transition :deal_river => :hand_over, :do => :determine_winner
		#after_transition any => :fold, :do => :set_winner 

		event :change_state do
	 		transition :waiting => :new_round,  
	 		:new_round => :deal_flop,
	 		:deal_flop => :deal_turn,
	 		:deal_turn => :deal_river,
	 		:deal_river => :hand_over,
	 		:hand_over => :new_round
	 	end

	 	event :end_hand do 
	 		transition :new_round => :fold,
	 		:first_betting => :fold,
	 		:deal_flop => :fold,
	 		:second_betting => :fold,
	 		:deal_turn => :fold,
	 		:third_betting => :fold,
	 		:deal_river => :fold,
	 		:fourth_betting => :fold
	 	end
	end


	def add_player(user)
		user.game_id = self.id
		self.users << user
		user.save
		self.save
	end

	def reset_attributes
		self.communal_cards = []
		self.bet = 10
		self.pot = 0
		self.winner = nil
		self.choice = nil
		self.deck = Deck.new()
		self.previous_choice = nil
		self.previous_bet = nil
	end

	def deal_cards
		reset_attributes
		self.deck.shuffle
		check_balance
		remove_funds_from_user
		self.pot = self.pot + (self.bet * self.users.length)
		self.users.each do |player|
			hand = Hand.new()
			hand.game_id = self.id
			hand.user_id = player.id
			deal_hole_card hand
			deal_hole_card hand
			hand.save
			self.hands << hand
		end
		self.save
	end

	def deal_flop_cards
		self.deck.shuffle
		self.communal_cards << self.deck.deal_flop
		self.save
	end

	def deal_turn_cards
		self.deck.shuffle
		self.communal_cards << self.deck.deal_turn
		self.save
	end

	def deal_river_cards
		self.deck.shuffle
		self.communal_cards << self.deck.deal_river
		self.save
	end

	def deal_hole_card hand
		hand.add_hole_card self.deck.deal_card
		hand.save
	end


	#hands.select {|hand| hand.state == 0 and hand.user_id == player.id}
	def determine_winner
		hands = self.hands.select {|hand| hand.state == 0}
		hands.each do |hand|
			hand.add_communal_cards = self.deck.communal_cards
			hand.save
		end
		hand_evaluator = HandEvaluator.new(self.users,self)
		self.winner = hand_evaluator.determine_winner
	end

	def set_winner
		self.users.each do |user|
			self.winner = user.id unless user.choice == "fold"
		end
		@winner.balance = @winner.balance + self.pot
	end

	def end_hand
		self.state = :hand_over
		self.winner = self.users.select {|user| user.id  != self.activeuser}[0]
	end

	def user_choice
		
		if self.choice == "fold"
			self.end_hand
		elsif self.choice == "check"
			if self.previous_choice.nil?
			 	self.previous_choice = self.choice
			else 
					self.change_state
					self.choice = nil
					self.previous_choice = nil
			end
		elsif self.choice == "call"
			remove_funds_from_user
			user.save
			self.change_state
			self.pot = self.pot + self.bet
			self.choice = nil
			self.bet = nil
			self.previous_bet.nil
		elsif self.choice == "bet" 
			check_balance
			remove_funds_from_user
			self.previous_bet = self.bet
		elsif self.choice == "raise"
			remove_funds_from_raising_user
			check_balance
			remove_funds_from_user
			self.previous_bet = self.bet
		elsif self.choice == nil
			self.change_state
			self.choice = "first_user_choice"
		end
		new_active_user = self.users.select {|user| user.id != self.activeuser}[0]
		self.activeuser = new_active_user.id
		self.save
	end

	def remove_funds_from_user
		user = self.users.select {|user| user.id == self.activeuser}[0]
		user = self.users[0] if user.nil?
		user.balance = user.balance - self.bet
		user.save
	end

	def remove_funds_from_raising_user
		user = self.users.select {|user| user.id == self.activeuser}[0]
		user.balance = user.balance - self.previous_bet
		user.save
	end

	def check_balance
		if self.bet > self.users[0].balance or self.bet >self.users[1].balance
			if self.users[0].balance > self.users[1].balance
				self.bet = self.users[1].balance
			else
				self.bet = self.users[0].balance	
			end
			self.choice = "allin"
			self.save
		end
	end		

end