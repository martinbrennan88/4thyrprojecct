class Card
	
	def initialize(number,suit)
		@number = number
		@suit = suit
	end
		
	def suit
		@suit
	end
		
	def number
		@number
	end

end
