class HandEvaluator

	#still to do: when a hand is a draw, why card numbers are appearing wrong????

	def initialize(player_list,game)
		@users = player_list
		@game = game
		@hand_list = {"straight flush" => 1,"four of a kind" => 2,"fullhouse" => 3,"flush" => 4,"straight" =>5,"three of a kind" =>6,"two pair" =>7,"pair" => 8,"high card" => 9}
	end

    #determines the winner of the hand of poker
    def determine_winner
    	winning_player = nil
    	winning_hand = nil
    	best_hand = nil
    	num = 0
    	@users.each do |player|
    		hand = @game.hands.select {|hand| hand.state == 0 and hand.user_id == player.id}
    		hand = hand[0]
    		best_hand,rank =  calculate_hand hand.cards
    		hand.rank = rank
    		hand.best_hand = best_hand
    		hand.state = 1
    		player.hands << hand
    		player.save
    		hand.save
    		if num == 0
	    		winning_player = player
	    		winning_hand = hand
	    	end
    		if @hand_list[rank] < @hand_list[winning_hand.rank]
    			winning_player = player
    			winning_hand = hand
    		elsif @hand_list[rank] == @hand_list[winning_hand.rank]
    			winning_player = player unless winning_hand.best_hand[0].number > hand.best_hand[0].number
    		end
    		num = num + 1
    	end
    	return winning_player.id
    end

    # calculateshand 
    def calculate_hand cards
    	@made_hand,rank,best_hand,straight_flush = false,"",[],[]
    	# sorts cards high to low 9,8,7,6, etc
    	cards.sort! {|x,y| y.number <=> x.number}
        best_flush = determine_suits cards
        unless best_flush.empty?
        	straight_flush = straight?(best_flush)
        end
        # if the straight_flush isnt empty
        unless straight_flush.empty?
        	best_hand = straight_flush
        	rank = "straight flush"
        else
        	sort_hand_ranks cards
		   	best_hand,rank = determine_best_possible_hand best_flush,cards
		end
		return best_hand,rank
	end


	def determine_best_possible_hand best_flush,cards
		high_card = []
		# if four_of a kind exists
		if @four_of_a_kind
			best_hand = hand_calculation @four_of_a_kind,cards
			rank = "four of a kind"
		# if there is a three of a kind AND another three of a kind to make a full house
		elsif @three_of_a_kind_1 and @three_of_a_kind_2
			three_of_a_kind_2.pop
			best_hand = three_of_a_kind_1 + three_of_a_kind_2
			rank = "fullhouse"
		        # if there is a three of a kind and a pair to make a full house
	    elsif @three_of_a_kind_1 and @pair_1
	    	best_hand = @three_of_a_kind_1 + @pair_1
	    	rank = "fullhouse"
	    else
	    	# if the flush isnt empty
	    	unless best_flush.empty?
	    		while best_flush.length > 5
	    			best_flush.pop
	    		end
	    		best_hand = best_flush
	    		rank = "flush"
	    	else
	    		straight = []
	    		straight = straight? cards
	    		# if the straight isnt empty
	    		if  !straight.empty?
	    			best_hand = straight
	    			rank = "straight"
	    			# if there is three of a kind
	    		elsif @three_of_a_kind_1 
	    			best_hand = hand_calculation @three_of_a_kind_1,cards
	    			rank = "three of a kind"
	    			#if pair one and pair two arent empty
	    		elsif @pair_1 and @pair_2
	    			# putting both pairs in the one variable pair_1
	    			@pair_1 = @pair_1 + @pair_2
	    			high_card = cards - @pair_1 
	    			add_high_cards(@pair_1,high_card)
	    			best_hand = @pair_1
	    			rank = "two pair"
	    			# if pair one isnt empty
	    		elsif @pair_1
	    			best_hand = hand_calculation @pair_1,cards
	    			rank = "pair"
	    		else
	    			while cards.length > 5
	    				cards.pop()
	    			end
	    			best_hand = cards
	    			rank = "high card"
	    		end
	    	end
	    end
	    return best_hand,rank
	end

	def sort_hand_ranks cards
		@four_of_a_kind,@three_of_a_kind_1,@three_of_a_kind_2,@pair_1,@pair_2 = nil,nil,nil,nil,nil
       	determine_multiples cards
       	@made_hand = false
       	i = 0
       	while(@made_hand == false)
        	get_hand @cards_array[i]
       		i = i + 1
	   		@made_hand = true if 12 == i
	    end
	end

	def hand_calculation hand,cards
		high_card = cards - hand  
		add_high_cards(hand,high_card)
		return hand
	end

	def add_high_cards(end_hand,cards_to_add)
		length = end_hand.length
		i = 0
		while(end_hand.length < 5)
			end_hand << cards_to_add[i]
			i = i + 1
		end
	end

	def get_hand array
    	unless array.empty?
	    	if array.length == 4 and @four_of_a_kind.nil?
		        @four_of_a_kind = array
		        @made_hand = true
		    elsif array.length == 3 and @three_of_a_kind_1.nil?
		    	@three_of_a_kind_1 = array
		    elsif array.length == 3 and @three_of_a_kind_2.nil?
		    	@three_of_a_kind_2 = array
		    	@made_hand = true
		    elsif array.length == 2 and @pair_1.nil?
		    	@pair_1 = array
		    	@made_hand = true if @three_of_a_kind_1
		    elsif array.length == 2 and @pair_2.nil?
		    	@pair_2 = array
		    end
		end
	end
    
  
     #after determining the the type of each card deciding the best hand the player can make called in calculate hand
    def straight?(cards)
        	# number player has to making straight
        number_to_straight = 0
        straight_cards = []
        previous_card = nil
        if cards[0].number == 14 and cards[cards.length-1].number == 2
	    	ace = cards[0]
	    	cards.push(Card.new(1,ace.suit))
	    end
        cards.each do |card|
        	# if youve already made a high straight skip the next few cards
        	unless number_to_straight == 5
	        	if previous_card.nil?
	        		number_to_straight = 1
	        		straight_cards << card
	        	elsif previous_card.number-1 == card.number
	        		number_to_straight = number_to_straight + 1
	        		straight_cards << card
	        	elsif previous_card.number != card.number
	        		straight_cards.clear
	                number_to_straight = 1
	                straight_cards << card
	            end
	            previous_card = card
	        end
	    end 
	    straight_cards.clear unless number_to_straight == 5
		if !straight_cards.empty? and straight_cards[0].number == 5 
	    	# make the ace 14 again and not 1
	    	straight_cards.pop
		    straight_cards.push(ace)
		end
	    return straight_cards
	end
    

    def determine_multiples cards
    	ace,two,three,four,five,six,seven,eight,nine,ten,jack,queen,king = [],[],[],[],[],[],[],[],[],[],[],[],[]
    	@cards_array = [ace,two,three,four,five,six,seven,eight,nine,ten,jack,queen,king]
    	cards.each do |card|
	    case card.number
	        when 14
	            ace << card
	        when 2
	            two << card
	        when 3
	            three << card
	        when 4
	            four << card
	        when 5
	            five << card
	        when 6
	            six << card
	        when 7
	           	seven << card
	        when 8
	            eight << card
	        when 9
	            nine << card 
	        when 10
	            ten << card
	        when 11
	            jack << card
	        when 12
	            queen << card
	        when 13
	            king << card
	        end
	    end
    end

    def determine_suits cards
    	diamond,spade,heart,club,flush = [],[],[],[],[]
	    cards.each do |card|
    		if card.suit == 'd'
    			diamond << card
    			flush = diamond if diamond.length == 5
    		elsif card.suit == 's'
    			spade << card
    			flush = spade if spade.length == 5
	    	elsif card.suit == 'h'
    			heart << card
    			flush = heart if heart.length == 5
	   		else card.suit == 'c'
	   			club << card
	   			flush = club if club.length == 5
	   		end
	    end
	    return flush
    end
end