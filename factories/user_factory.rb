FactoryGirl.define do 
	factory :user do
		first_name 'dave'
    	last_name 'tester'
    	username 'Nice_guy'
    	email 'dave@hotmail.com'
    	password 123456
    	confirm_password 123456	
    end
end