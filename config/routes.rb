Project::Application.routes.draw do

  authenticated :user do
    root :to => 'home#signed_in'
  end

  root :to => "home#index"

  match "/top_up" => "users#top_up", :as => :top_up
  match "games/:id/hands/" => "games#get_current_hands", :as => :current_hand
  match "games/:id/cards/" => "games#get_communal_cards", :as => :communal_cards

  devise_for :users, :controllers => { :registrations => "registrations" }
  resources :users
  resources :games
  resources :hands
end